import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:movie_flutter_app/app/Constants.dart';
import 'package:movie_flutter_app/app/app_theme.dart';
import 'package:movie_flutter_app/mvp/model/movie_model.dart';

class MovieDetail extends StatelessWidget{

  final Result mMovieItem;
  MovieDetail(this.mMovieItem);
  Color mainColor = const Color(0xff3C3261);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
          fit: StackFit.expand,
          children: [
            new Image.network("${Constants.IMG_BASE_URL}${mMovieItem.backdropPath}",fit: BoxFit.cover,),
            new BackdropFilter(
              filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
              child: new Container(
                color: Colors.black.withOpacity(0.5),
              )
              ,),
            new SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.only(top: 16),
                margin: const EdgeInsets.all(20.0),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      alignment: Alignment.center,
                      child: new Container(width: 400.0,height: 400.0,),
                      decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.circular(10.0),
                          image: new DecorationImage(image: new NetworkImage("${Constants.IMG_BASE_URL}${mMovieItem.posterPath}"),fit: BoxFit.cover),
                          boxShadow: [
                            new BoxShadow(
                                color: Colors.black,
                                blurRadius: 20.0,
                                offset: new Offset(0.0, 10.0)
                            )
                          ]
                      ),
                    ),
                    new Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0,horizontal: 0.0),
                      child: new Row(

                        children: <Widget>[
                          new Expanded(child: new Text(mMovieItem.title,style: new TextStyle(color: Colors.white,fontSize: 30.0,fontFamily: AppTheme.font_text),)),
                          new Text('${mMovieItem.voteAverage}/10',style: new TextStyle(color: Colors.white,fontSize: 20.0,fontFamily: AppTheme.font_text),)
                        ],
                      ),
                    ),
                    new Text(mMovieItem.overview,style: new TextStyle(color: Colors.white,fontFamily: AppTheme.font_text)),
                    new Padding(padding: const EdgeInsets.all(10.0)),
                    Row(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.language,
                              color: Colors.blueAccent,
                            ),
                            Text(
                              "${mMovieItem.originalLanguage}",
                              style: new TextStyle(
                                  color: AppTheme.color_primary_light,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                  fontFamily: AppTheme.font_text),
                            ),
                          ],
                        ),SizedBox(width: 20,),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.thumb_up,
                              color: Colors.green,
                            ),
                            Text(
                              "${mMovieItem.voteCount}",
                              style: new TextStyle(
                                  color: AppTheme.color_primary_light,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                  fontFamily: AppTheme.font_text),
                            ),
                          ],
                        ),SizedBox(width: 20,),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                            Text(
                              "${mMovieItem.voteAverage}",
                              style: new TextStyle(
                                  color: AppTheme.color_primary_light,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12.0,
                                  fontFamily: AppTheme.font_text),
                            ),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ]
      ),
    );
  }
}