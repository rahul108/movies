import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:movie_flutter_app/app/Constants.dart';
import 'package:movie_flutter_app/app/app_theme.dart';
import 'package:movie_flutter_app/base/base_util.dart';
import 'package:movie_flutter_app/mvp/model/movie_model.dart';
import 'package:movie_flutter_app/mvp/presenter/movie_presenter.dart';

import 'movie_detail.dart';

class MoviesPage extends StatefulWidget {
  MoviesPage({Key key}) : super(key: key);

  @override
  _MoviesPageState createState() => _MoviesPageState();
}

class _MoviesPageState extends State<MoviesPage> with BaseUtil {
  MoviePresenter presenter;
  Future<List<Result>> movieResultList;
  bool isInternetConnected = true;
  TextEditingController _searchControler = new TextEditingController();

  @override
  initState() {
    super.initState();

    presenter = new MoviePresenter();
    presenter.initContext = context;
    initialCalls();
  }

  initialCalls() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    checkInternetConnection(context).then((onValue) {
      if (onValue) {
        setState(() {
          isInternetConnected = onValue;
          print("Search 1 : ${_searchControler.text}");
          if (_searchControler.text.isNotEmpty) {
            movieResultList = presenter.getMovieList("SEARCH",_searchControler.text);
          } else {
            movieResultList = presenter.getMovieList("MOVIE","");
          }
        });
      } else {
        setState(() {
          isInternetConnected = onValue;
        });
      }
    });
  }

  @override
  void dispose() {
    _searchControler.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => isInternetConnected
      ? Scaffold(
          appBar: AppBar(
            title: Text("Now Playing Movies"),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 9,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: TextFormField(
                          autofocus: false,
                          style: TextStyle(
                              fontFamily: AppTheme.font_text, fontSize: 14.0),
                          decoration: InputDecoration(
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 10.0),
                              labelText: "Search movies Here",
                              border: OutlineInputBorder()),
                          controller: _searchControler,
                          keyboardType: TextInputType.text,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: IconButton(icon: Icon(Icons.search),onPressed: (){
                                initialCalls();
                                SystemChannels.textInput.invokeMethod('TextInput.hide');
                        },),
                      ),
                    )
                  ],
                ),
                FutureBuilder<List<Result>>(
                    future: movieResultList,
                    builder: (context, mMovieResults) {
                      if (mMovieResults.hasData) {
                        print("Results");
                        return mMovieResults.data != null
                            ? _buildMoviesList(mMovieResults.data)
                            : responseWidget(
                                btnCaption: "Try again",
                                message: "Movies not found.");
                      }
                      return loadingWidget();
                    }),
              ],
            ),
          ),
        )
      : responseWidget(
          btnCaption: "Internet Connection",
          message: "Internet or WiFi Connection is not connected");

  @override
  responseLoaderInterface() {
    setState(() {
      initialCalls();
    });
  }

  _buildMoviesList(List<Result> moviesResults) {
    print("ok in ${moviesResults.length}");
    return Container(
      child: ListView.builder(
          itemCount: moviesResults.length,
          physics:  NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            var mMovieItem = moviesResults[index];
            return InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MovieDetail(mMovieItem)));
              },
              child: Card(
                elevation: 5.0,
                margin: EdgeInsets.all(6),
                color: Colors.white,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      width: 100,
                      height: 150,
                      alignment: Alignment.center,
                      child: CachedNetworkImage(
                        imageUrl:"${Constants.IMG_BASE_URL}${mMovieItem.posterPath}",
                        placeholder: (context, url) => new CircularProgressIndicator(),
                        width: 100,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: 200,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                             Text(
                                mMovieItem.title,
                                maxLines: 2,
                                style: new TextStyle(
                                    color: AppTheme.color_primary_text,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18.0,
                                    fontFamily: AppTheme.font_text),
                            ),
                            Text(
                              "Release date : ${mMovieItem.releaseDate}",
                             // "Release date : ${DateFormat('yyyy-MM-dd').format(mMovieItem.releaseDate)}",
                              style: new TextStyle(
                                  color: AppTheme.color_secondary_text,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16.0,
                                  fontFamily: AppTheme.font_text),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.language,
                                      color: Colors.blueAccent,
                                    ),
                                    Text(
                                      "${mMovieItem.originalLanguage}",
                                      style: new TextStyle(
                                          color: AppTheme.color_secondary_text,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14.0,
                                          fontFamily: AppTheme.font_text),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.thumb_up,
                                      color: Colors.green,
                                    ),
                                    Text(
                                      "${mMovieItem.voteCount}",
                                      style: new TextStyle(
                                          color: AppTheme.color_secondary_text,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14.0,
                                          fontFamily: AppTheme.font_text),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      color: Colors.red,
                                    ),
                                    Text(
                                      "${mMovieItem.voteAverage}",
                                      style: new TextStyle(
                                          color: AppTheme.color_secondary_text,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12.0,
                                          fontFamily: AppTheme.font_text),
                                    ),
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
