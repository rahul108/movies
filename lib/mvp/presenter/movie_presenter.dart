import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:movie_flutter_app/api/api_route.dart';
import 'package:movie_flutter_app/app/Constants.dart';
import 'package:movie_flutter_app/mvp/model/movie_model.dart';
import 'package:movie_flutter_app/utility/toast_utility.dart';

class MovieInterfacePresenter {
  set initContext(BuildContext context) {}
}

class MoviePresenter implements MovieInterfacePresenter {
  BuildContext _context;

  @override
  void set initContext(BuildContext context) {
    _context = context;
  }

  Future<List<Result>> getMovieList(String event, String searchText) async {
    var mMovieResults;
    try {
      http.Response response = null;
      if (event == "MOVIE") {
        response = await ApiCall(_context).getApiCall(Constants.NOW_PLAYING);
      } else {
        response = await ApiCall(_context)
            .getApiCall("${Constants.SEARCH_MOVIE}$searchText");
      }
      print("Ok Search 6 : ${Constants.SEARCH_MOVIE}$searchText");
      if (response != null) {
        var _data = json.decode(utf8.decode(response.bodyBytes));
        MoviesModel moviesModel = MoviesModel.fromJson(_data);
        if (moviesModel.results.isNotEmpty) {
          mMovieResults = moviesModel.results;
        } else {
          mMovieResults = null;
        }
      } else {
        mMovieResults = null;
      }
    } catch (e) {
      print("Ok error : $e");
      ToastUtility.infoToast(
          "Could not load data. Please try after few minutes");
      mMovieResults = null;
    }
    return mMovieResults;
  }
}
