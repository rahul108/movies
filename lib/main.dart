import 'package:flutter/material.dart';
import 'package:movie_flutter_app/app/app_theme.dart';
import 'package:movie_flutter_app/mvp/view/movie_view_componant.dart';

void main() => runApp(MovieApp());

class MovieApp extends StatefulWidget {
  @override
  MovieAppState createState() {
    return new MovieAppState();
  }
}

class MovieAppState extends State<MovieApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MoviesFlutterApp',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        backgroundColor: AppTheme.color_primary_light,
        primaryColor: AppTheme.color_primary_dark,
        primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
      ),
      home: MoviesPage(),
    );
  }
}
