import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:movie_flutter_app/utility/flip_card.dart';
import 'package:movie_flutter_app/utility/internet_connection.dart';
import 'package:movie_flutter_app/utility/toast_utility.dart';

mixin BaseUtil {
  bool _isConnect;

  Future<bool> checkInternetConnection(BuildContext context) async {
    await isInternetConnected().then((value) {
      if (!value) {
        ToastUtility.errorToast("Internet or WiFi Connection is not connected");
      }
      _isConnect = value;
    });
    return _isConnect;
  }

  Widget loadingWidget() {
    return Center(
      child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FlipCard(
              back: Image.asset(
                "assets/icon.png",
                width: 75.0,
                height: 75.0,
              ),
              front: Image.asset(
                "assets/icon.png",
                width: 75.0,
                height: 75.0,
              ),
              direction: FlipDirection.HORIZONTAL,
              flipOnTouch: false,
              speed: 500,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 4.0),
              child: Wrap(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Please wait",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14.0,
                              fontFamily: "sans-sarif",
                              fontWeight: FontWeight.w500)),
                    ],
                  ),
                ],
              ),
            ),
          ]),
    );
  }

  Widget responseWidget({IconData icon, String message, String btnCaption}) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            icon != null ? icon : Icons.warning,
            size: 60.0,
            color: Colors.black54,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text(
              message,
              style: TextStyle(
                  color: Colors.black54,
                  fontFamily: "sans-sarif",
                  fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: RaisedButton(
              child: Text(
                btnCaption,
                style: TextStyle(fontFamily: "sans-sarif"),
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                responseLoaderInterface();
              },
              textColor: Colors.white,
              color: Colors.blue,
            ),
          ),
        ],
      ),
    );
  }

  void responseLoaderInterface() {}
}
