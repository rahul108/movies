class Constants {

  //Base Url
  static const BASE_URL = "https://api.themoviedb.org/3";
  static const IMG_BASE_URL = "https://image.tmdb.org/t/p/w500";
  static const API_KEY = "7fb661b071bc9174031df3a60813ab7f";

  //End Points
  static const String NOW_PLAYING = "$BASE_URL/movie/now_playing?api_key=7fb661b071bc9174031df3a60813ab7f";
  static const String SEARCH_MOVIE = "$BASE_URL/search/movie?api_key=7fb661b071bc9174031df3a60813ab7f&query=";
}
