import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  /*App Colors*/
  static const Color color_primary = Color(0xFF2D373F);
  static const Color color_primary_dark = Color(0xFF2D373F);
  static const Color color_primary_light = Color(0xFFEEEEEE);
  static const Color color_accent = Color(0xFF147F7F);

  static const Color color_primary_text = Color(0xFF212121);
  static const Color color_secondary_text = Color(0xFF757575);

  static const String font_text = 'sans-serif';
}
