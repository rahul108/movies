import 'package:connectivity/connectivity.dart';

Future<bool> isInternetConnected() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    print("Connect OK");
    return true;
  } else {
    return false;
  }
}
