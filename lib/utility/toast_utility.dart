import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastUtility {
  static void normalToast(String message) {
    toastProperties(message, Colors.indigoAccent, Colors.white);
  }

  static void infoToast(String message) {
    toastProperties(message, Colors.purpleAccent, Colors.white);
  }

  static void successToast(String message) {
    toastProperties(message, Colors.green, Colors.white);
  }

  static void warnToast(String message) {
    toastProperties(message, Colors.yellow, Colors.white);
  }

  static void errorToast(String message) {
    toastProperties(message, Colors.redAccent, Colors.white);
  }

  static void toastProperties(
      String message, Color backColor, Color textColor) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: backColor,
        textColor: textColor,
        fontSize: 16.0);
  }
}
