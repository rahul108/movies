import 'dart:io';
import 'dart:async';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'api_client.dart';

class ApiCall {

  static const String CONTENT_TYPE = "application/json";

  RestApiClient restApiClient;
  BuildContext context;

  ApiCall(BuildContext context) {
    restApiClient = RestApiClient();
    this.context = context;
  }

  Future<dynamic> getApiCall(String url) async {
    final http.Response response = await restApiClient.getHttpResponse(
      context,
      url,
      headers: {
        HttpHeaders.contentTypeHeader: CONTENT_TYPE,
      },
      method: HttpMethod.get,
    );
    return response;
  }
}
