import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart' as client;
import 'package:logger/logger.dart';
import 'package:movie_flutter_app/utility/toast_utility.dart';

enum HttpMethod { get, post, put, patch, delete }

class RestApiClient {
  var logger = Logger();

  Future<http.Response> getHttpResponse(
    BuildContext context,
    String url, {
    dynamic body,
    Map<String, String> headers,
    HttpMethod method,
  }) async {
    logger.i("REQUEST URL : $url ");
    logger.i("REQUEST BODY : $body ");

    final client.IOClient _client = getClient();
    http.Response response;
    try {
      switch (method) {
        case HttpMethod.post:
          response = await _client.post(
            url,
            body: body,
            headers: headers,
          );
          break;

        case HttpMethod.put:
          response = await _client.put(
            url,
            body: body,
            headers: headers,
          );
          break;

        case HttpMethod.patch:
          response = await _client.patch(
            url,
            body: body,
            headers: headers,
          );
          break;

        case HttpMethod.delete:
          response = await _client.delete(
            url,
            headers: headers,
          );
          break;

        case HttpMethod.get:
          response = await _client.get(
            url,
            headers: headers,
          );
          break;
      }
    } finally {
      logger.i("Request Headers : ${headers}");
      logger.i("Headers : ${response.headers}");
      logger.i("STATUS CODE : ${response.statusCode}");
      logger.i("RESPONSE BODY : ${utf8.decode(response.bodyBytes)}");
    }

    if (response.statusCode == 200) {
      return response;
    } else {
      String message = json.decode(utf8.decode(response.bodyBytes))["status_message"];
      logger.i("MESSAGE : $message");
      negativeMessage(message, context);
    }
    return null;
  }
}

client.IOClient getClient() {
  bool trustSelfSigned = true;
  HttpClient httpClient = new HttpClient()
    ..badCertificateCallback =
        ((X509Certificate cert, String host, int port) => trustSelfSigned);
  client.IOClient ioClient = new client.IOClient(httpClient);
  return ioClient;
}

void negativeMessage(String message, BuildContext context) {
  ToastUtility.errorToast(message);
}
